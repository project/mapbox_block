# Mapbox Block

Provides a configurable block to render and display a Mapbox map.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/mapbox_block).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/mapbox_block).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

Requires Mapbox access token:
`https://docs.mapbox.com/help/getting-started/access-tokens/`

Requires the Key module:
- [Key](https://www.drupal.org/project/key)

Requires Mapbox GL JS v2. Doesn't work with older versions.
`https://docs.mapbox.com/mapbox-gl-js/guides/migrate-to-v2/`


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Configure Mapbox Access token: **/admin/config/mapbox-block**
2. Goto `'Block layout'` page and add block to specified region.
3. Find `"Mapbox Map"` block and configure.
4. Save block after configuration.
5. Your map is now rendered on the page.


## Maintainers

- George Anderson - [geoanders](https://www.drupal.org/u/geoanders)
- Michael O'Hara - [mikeohara](https://www.drupal.org/u/mikeohara)
